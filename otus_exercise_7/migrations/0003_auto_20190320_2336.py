# Generated by Django 2.1.7 on 2019-03-20 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('otus_exercise_7', '0002_auto_20190320_2334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
