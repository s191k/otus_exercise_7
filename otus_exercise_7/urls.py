from django.conf.urls import url
from django.urls import include
from django.contrib import admin

from . import views

urlpatterns = [
    url('^$', views.main_page, name='index'),
    url('^index/$', views.main_page, name='index'),
    #sort_questions_on_main_page
    url('^index/sort_by_date$', views.order_questions_by_new, name='index'),
    url('^index/sort_by_votes$', views.order_questions_by_hot, name='index'),


    url('^answerQuestion/$', views.answerQuestion_page, name='answerQuestion'),

    url('^askQuestion/$', views.askQuestion_page, name='askQuestion'),
    url('^logged_user/$', views.logged_user_page, name='logged_user'),
    url('^login/$', views.login_page, name='login'),
    url('^searchresult/$', views.searchresult_page, name='searchresult'),
    url('^searchtag/$', views.searchtag_page, name='searchtag'),
    url('^signUp/$', views.signUp_page, name='signUp'),
    url('^user_settings/$', views.user_settings_page, name='user_settings'),

    url('admin/', admin.site.urls),

]
