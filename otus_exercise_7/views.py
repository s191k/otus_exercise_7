from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import *

from otus_exercise_7.forms import LogInForm
from .models import Question

def main_page(request):
    questions = Question.objects.all() ## Везде тяну для Trendings - блок на правой сторона сайта
    return render(request, 'index.html', locals())

def answerQuestion_page(request):
    questions = Question.objects.all()
    answers = Answer.objects.all()
    form = AnswerForm(request.POST or None)
    # # answers = Answer.objects.filter(question='O')
    if request.method == "POST" and form.is_valid():
        data = form.cleaned_data
        form.save()  ## Сохраняем данные

    return render(request, 'answerQuestion.html', locals())

def order_questions_by_new(request):
    questions = Question.objects.all().order_by('votes')
    # trend_questionns = Question.objects.all().order_by('votes') # Сортировать всегда по убыванию голосов
    return render(request, 'index.html', locals())

def order_questions_by_hot(request):
    questions = Question.objects.all().order_by('create_date') ## Везде тяну для Trendings - блок на правой сторона сайта
    return render(request, 'index.html', locals())

def askQuestion_page(request):
    questions = Question.objects.all()
    form = AskQuestionForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        data = form.cleaned_data ##?? Зачем это
        print(data)
        print(form)
        data['author'] = 'test111'
        print(data)
        form.save()  ## Сохраняем данные
        return redirect('/index')

    return render(request, 'askQuestion.html', locals())

def logged_user_page(request):
    questions = Question.objects.all()
    return render(request, 'settings_logged.html', locals())

def login_page(request):
    questions = Question.objects.all()
    form = LogInForm(request.POST or None)
    user = User()
    user.nickname='usetTest'
    if request.method == "POST" and form.is_valid():
        user_name = form.cleaned_data.get('nickname')
        user_password = form.cleaned_data.get('password')
        user = User.objects.all().filter(nickname=user_name).filter(password=user_password) ##QuerySet
        if len(user) == 1:
            # return redirect('/index/')
            user = user[0]
            return render(request, 'index.html', locals())
        else:
            error = 'No such user in base. Check nickname or password.'

    return render(request, 'login.html', locals())

def logout_view(request):
    logout(request)

def searchresult_page(request):
    steck = '<TextHere>' ##??
    questions = Question.objects.all()
    return render(request, 'searchresult.html', locals())

def searchtag_page(request):
    questions = Question.objects.all()
    return render(request, 'searchtag.html', locals())

def signUp_page(request):
    questions = Question.objects.all()
    form = SignUpForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        data = form.cleaned_data
        form.save() ## Сохраняем данные

    return render(request, 'signUp.html', locals())

def user_settings_page(request):
    questions = Question.objects.all()
    return render(request, 'user_settings.html', locals())


