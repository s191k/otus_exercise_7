import datetime
from django.db import models


# py ./manage.py makemigrations otus_exercise_7


class User(models.Model):
    email = models.EmailField(max_length=128)
    nickname = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    avatar = models.ImageField(default='/static/img/avatar.jpg')  # нет автара - тогда дефолт
    registration_date = models.DateField(default=datetime.date.today)

    def __str__(self):
        return self.nickname


class Answer(models.Model):
    answerText = models.CharField(max_length=128)
    author = models.CharField(max_length=128)
    create_date = models.DateField(default=datetime.date.today)
    is_answer_choosed = models.BooleanField(default=False)
    # question = models.CharField(max_length=128) ## Ссылка на question?
    # question = models.ForeignKey(Question, related_name='+', on_delete=models.CASCADE) ##??? Правильная связка?
    # main_answer = models.BooleanField(default=False) &&???
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.answerText[:20]  # Первые 20 символов ответа.(Для админки).


class Question(models.Model):
    title = models.CharField(max_length=128)
    question_body = models.CharField(max_length=128)
    author = models.CharField(max_length=128, null=True, blank=True)
    create_date = models.DateField(default=datetime.date.today)
    tags = models.CharField(max_length=128, null=True, blank=True)  ## Вот тут array

    # new
    # votes = models.IntegerField(default=0)  # Тянем из Answer
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE) # Смотрим количество ответов и копаемся в них

    def __str__(self):
        return self.title


class Tag(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name
