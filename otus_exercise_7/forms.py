import datetime

from django import forms
from .models import *


class LogInForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ["email", "avatar", "registration_date"]


class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        # User.registration_date = datetime.datetime.now()
        exclude = ["registration_date"]
        # exclude = []


class AskQuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = ["author", "create_date", "votes", "answers"]


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = ["author", "create_date", "is_answer_choosed"]
